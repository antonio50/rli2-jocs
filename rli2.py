##################################################################
# 
# <Main Class>
# 
# RLI2 - Red Lor Invader for Reinforcement Learning Interaction
# by Antonio Horta --- 2023
# ajhorta@cybercrafter.com.br
#
##################################################################

from constants import *
import copy
import os
import yaml
from yaml.loader import SafeLoader
import json
import csv
import tempfile

import gym
import nasim
import random
import numpy as np
import pandas as pd
from statistics import mean

import shutil
import hashlib


from stable_baselines3 import A2C, DQN, PPO 
from sb3_contrib import RecurrentPPO, TRPO, QRDQN
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.callbacks import BaseCallback

from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.evaluation import evaluate_policy

from imitation.algorithms.adversarial.gail import GAIL
from imitation.algorithms import bc
from imitation.data import rollout
from imitation.data.wrappers import RolloutInfoWrapper
from imitation.algorithms.dagger import SimpleDAggerTrainer
from imitation.util.networks import RunningNorm
from imitation.rewards.reward_nets import BasicRewardNet
from imitation.policies.serialize import load_policy

class RLI2():

    # init method or constructor
    def __init__(self):
        self.list_results = []

    # Get list of sceneries from constants and nasim benchmark dir
    def get_scenery_list(self):
      
        nasim_benchmark_path = os.path.join("benchmark")
        
        scenery_list = []
        for x in os.listdir(nasim_benchmark_path):
            if x.endswith(".yaml"):
                # Append files in SCENERY_LIST
                scenery_list.append('NASIM:'+x.replace(".yaml", ''))
                

        # return SCENERY_LIST + scenery_list
        return scenery_list

    # Get list of sceneries from constants and nasim benchmark dir
    def get_essay_list(self):
      
        results_path = os.path.join("results")
        
        essay_list = ["Empty"]
        for x in os.listdir(results_path):
            if x.endswith(".json"):
                # Append files in SCENERY_LIST
                essay_list.append('NASIM:'+x.replace(".json", ''))
                

        # return SCENERY_LIST + scenery_list
        return essay_list
 
    # Perform essay/attack
    def attack(self, scenery, series_list, timesteps, episode_steps):

        # remove old files and create new results    
        path = "results"
        # Check whether the specified path exists or not
        isExist = os.path.exists(path)
        if not isExist:
            # Create a new directory b['PrivilegeEscalation', 'Exploit', 'PrivilegeEscalation', 'Exploit', 'PrivilegeEscalation', 'Exploit', 'PrivilegeEscalation', 'Exploit', 'SubnetScan', 'PrivilegeEscalation', 'SubnetScan', 'PrivilegeEscalation', 'Exploit', 'PrivilegeEscalation', 'Exploit', 'PrivilegeEscalation', 'SubnetScan', 'Exploit']ecause it does not exist
            os.makedirs(path)
   
        # Check whether the specified path exists or nottracker_callback
        trained_models_path = "trained_models"
        isExist = os.path.exists(trained_models_path)
        if not isExist:
            # Create a new directory because it does not exist
            os.makedirs(trained_models_path)
            os.makedirs(os.path.join(path,trained_models_path))
   


        results_path = os.path.join(path,"results.json")
        finished_path = os.path.join(path,"finished.txt")
        if os.path.exists(results_path):
            os.remove(results_path)
        if os.path.exists(finished_path):
            os.remove(finished_path)
        
        
        # Start RL
        list_results = []
        
        for alg in series_list:

            total_timesteps = timesteps
            max_episode_steps = episode_steps

            tracker_callback = CustomCallback()

            # invert to generic or force a scenario
            scenery_tl = scenery + '-generic'
            
            if 'generic' in scenery:
                scenery_tl = scenery.replace('-generic','')
            
            # workaround for non exist scenarios generic
            if scenery == 'medium' \
                or scenery == 'medium-single-site' \
                or scenery == 'small' \
                or scenery == 'small-linear': 
                scenery_tl = scenery

            env = Monitor(nasim.make_benchmark(scenery, flat_actions=True, fully_obs=False))
            env_tl = Monitor(nasim.make_benchmark(scenery_tl, flat_actions=True, fully_obs=False))
            env_tl_dummy = Monitor(nasim.make_benchmark(scenery_tl, flat_actions=True, fully_obs=False))
            
            model_trained_path = os.path.join(trained_models_path, f'model-env-{scenery}-{alg}-{total_timesteps}-{max_episode_steps}')
            model_trained_path_tl = os.path.join(trained_models_path, f'model-env-tl-{scenery_tl}-{alg.split("+")[0] + "+TL"}-{total_timesteps}-{max_episode_steps}')

            model_trained_tl = self.check_trained_model(alg.split("+")[0] + "+TL", model_trained_path_tl)
            seed = 2023
            learning_rate = 0.001
                
            if "DQN" == alg:                
                env.reset()
                model = DQN("MlpPolicy", env, verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), target_update_interval = max_episode_steps, learning_rate=learning_rate)
            elif "DQN+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = DQN("MlpPolicy", env_tl, verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), target_update_interval = max_episode_steps, learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_dqn")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = DQN.load("model_dqn", env)
            elif "DQN+BC" == alg or "DQN+DAgger" == alg or "DQN+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = DQN("MlpPolicy", env_tl, verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), target_update_interval = max_episode_steps, learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)    
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
                
            elif "QRDQN" == alg:
                env.reset()
                model = QRDQN("MlpPolicy", env, policy_kwargs=dict(n_quantiles=50), verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), learning_rate=learning_rate)
            elif "QRDQN+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = QRDQN("MlpPolicy", env_tl, policy_kwargs=dict(n_quantiles=50), verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_qrdqn")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = QRDQN.load("model_qrdqn", env)
            elif "QRDQN+BC" == alg or "QRDQN+DAgger" == alg or "QRDQN+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = QRDQN("MlpPolicy", env_tl, policy_kwargs=dict(n_quantiles=50), verbose=1, train_freq=(max_episode_steps, "step"), seed=seed, learning_starts = int(total_timesteps*0.10), learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
            
            elif "A2C" == alg:
                env.reset()
                model = A2C("MlpPolicy", env, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, learning_rate=learning_rate)
            elif "A2C+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = A2C("MlpPolicy", env_tl, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_a2c")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = A2C.load("model_a2c", env)
            elif "A2C+BC" == alg or "A2C+DAgger" == alg or "A2C+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = A2C("MlpPolicy", env_tl, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
            
            elif "TRPO" == alg:
                env.reset()
                model = TRPO("MlpPolicy", env, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
            elif "TRPO+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = TRPO("MlpPolicy", env_tl, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_trpo")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = TRPO.load("model_trpo", env)
            elif "TRPO+BC" == alg or "TRPO+DAgger" == alg or "TRPO+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = TRPO("MlpPolicy", env_tl, gamma=0.9, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
                
            elif "PPO" == alg:
                env.reset()
                model = PPO("MlpPolicy", env, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
            elif "PPO+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = PPO("MlpPolicy", env_tl, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_ppo")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = PPO.load("model_ppo", env)
            elif "PPO+BC" == alg or "PPO+DAgger" == alg or "PPO+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = PPO("MlpPolicy", env, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
            
            elif "RecurrentPPO" == alg:
                env.reset()
                model = RecurrentPPO("MlpLstmPolicy", env, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
            elif "RecurrentPPO+TL" == alg:
                env_tl.reset()
                if model_trained_tl is not None:
                    model = model_trained_tl
                else:
                    model = RecurrentPPO("MlpLstmPolicy", env_tl, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    model.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path_tl)    
                model.save("model_recurrentppo")
                del model  # delete trained model to demonstrate loading with new env
                env.reset()
                model = RecurrentPPO.load("model_recurrentppo", env)
            elif "RecurrentPPO+BC" == alg or "RecurrentPPO+DAgger" == alg or "RecurrentPPO+GAIL" == alg:
                env_tl.reset()
                env_tl_dummy.reset()
                if model_trained_tl is not None:
                    expert = model_trained_tl
                else:
                    expert = RecurrentPPO("MlpLstmPolicy", env_tl, verbose=1, n_steps=max_episode_steps, seed=seed, batch_size=max_episode_steps, learning_rate=learning_rate)
                    expert.learn(total_timesteps=total_timesteps, log_interval=max_episode_steps, reset_num_timesteps=False)
                    expert.save(model_trained_path_tl)
                list_results += self.imitation(env_tl, env, env_tl_dummy, expert, total_timesteps, max_episode_steps, alg)
            else:
                print(alg, "Algorithm not implemented")
                continue

            if "+BC".lower() not in alg.lower() and "+DAgger".lower() not in alg.lower() and "+GAIL".lower() not in alg.lower():
                
                model_trained = self.check_trained_model(alg, model_trained_path=model_trained_path)
                trained_results_path = os.path.join(path,f'{model_trained_path}.json')
                    
                if model_trained is not None:
                
                    model = model_trained
                    with open(trained_results_path, "r") as results_file:
                        tracker_callback.list_results = json.load(results_file)
                
                else:
                
                    model.learn(total_timesteps=total_timesteps, callback=tracker_callback, log_interval=max_episode_steps, reset_num_timesteps=False)
                    model.save(model_trained_path)
                
                    # Put Correct alg when +TL +Imit are found
                    for r in tracker_callback.list_results:
                        r['serie'] = alg
                
                    with open(trained_results_path, "w") as write_file:
                        json.dump(tracker_callback.list_results, write_file)


                list_results += tracker_callback.list_results

            env.close()
            env_tl.close()
            env_tl_dummy.close()
        
        results_path_json = results_path
        
        with open(results_path_json, "w") as write_file:
            json.dump(list_results, write_file)


        algs_str = "".join(map(str,series_list)) 
        hash_object = hashlib.md5(algs_str.encode())
        algs_hash_str = scenery + "-" + str(timesteps) + "-" +  str(max_episode_steps) + "-" + str(hash_object.hexdigest())
        shutil.copyfile(results_path_json, results_path_json.replace(".json",f'-{algs_hash_str}.json'))            
        

        with open(finished_path, 'w') as fp:
            pass


    # load trained models
    def check_trained_model(self, alg, model_trained_path):
        
        ALG_DICT = {
            'DQN': DQN,
            'QRDQN': QRDQN,
            'A2C': A2C,
            'TRPO': TRPO,
            'PPO': PPO,
            'RecurrentPPO': RecurrentPPO,
            'DQN+TL': DQN,
            'QRDQN+TL': QRDQN,
            'A2C+TL': A2C,
            'TRPO+TL': TRPO,
            'PPO+TL': PPO,
            'RecurrentPPO+TL': RecurrentPPO,
             }
        trained_model = None
        if os.path.exists(model_trained_path + '.zip'):
                trained_model = ALG_DICT[alg].load(model_trained_path)            
        return trained_model

    # imitation algs
    def imitation(self, env_tl, env, env_tl_dummy, expert, timesteps, max_episode_steps, alg): 

        rng = np.random.default_rng(0)
                
        venv = DummyVecEnv([lambda: env_tl_dummy])
        
        env_tl.reset()
        rollouts = rollout.rollout(
            expert,
            DummyVecEnv([lambda: RolloutInfoWrapper(env_tl)]),
            rollout.make_sample_until(min_timesteps=max_episode_steps, min_episodes=int(timesteps/max_episode_steps)),
            rng=rng,                        
        )
        

        if '+BC'.lower() in alg.lower():

            transitions = rollout.flatten_trajectories(rollouts)
    
            env_tl.reset()                
            bc_trainer = bc.BC(
                observation_space=env_tl.observation_space,
                action_space=env_tl.action_space,
                demonstrations=transitions,
                rng=rng,
                batch_size=max_episode_steps                    
            )

            bc_trainer.train(n_epochs=1)
            trainer = bc_trainer.policy

        elif '+DAgger'.lower() in alg.lower():
            env_tl.reset()                
            bc_trainer = bc.BC(
                observation_space=env_tl.observation_space,
                action_space=env_tl.action_space,
                rng=rng, 
                batch_size=max_episode_steps
            )

            with tempfile.TemporaryDirectory(prefix="dagger_trainning_tmp_") as tmpdir:
                dagger_trainer = SimpleDAggerTrainer(
                    venv=venv,
                    scratch_dir=tmpdir,
                    expert_policy=expert,
                    bc_trainer=bc_trainer,
                    rng=rng,
                )
                dagger_trainer.train(total_timesteps=int(timesteps/100), rollout_round_min_episodes=int((timesteps/max_episode_steps)))
                trainer = dagger_trainer.policy
        
        elif '+GAIL'.lower() in alg.lower():
            if 'DQN'.lower() in alg.lower():
                learner = DQN("MlpPolicy", venv)
            elif 'QRDQN'.lower() in alg.lower():
                learner = QRDQN("MlpPolicy", venv)
            elif 'A2C'.lower() in alg.lower():
                learner = A2C("MlpPolicy", venv)
            elif 'TRPO'.lower() in alg.lower():
                learner = TRPO("MlpPolicy", venv)
            elif 'RecurrentPPO'.lower() in alg.lower():
                learner = RecurrentPPO("MlpLstmPolicy", venv)
            elif 'PPO'.lower() in alg.lower():
                learner = PPO("MlpPolicy", venv)
            
            reward_net = BasicRewardNet(
                venv.observation_space,
                venv.action_space,
                normalize_input_layer=RunningNorm,
            )

            n = int(timesteps/100)
            if n < 2048:
                n=2048 

            gail_trainer = GAIL(
                demonstrations=rollouts,
                demo_batch_size=max_episode_steps,
                gen_replay_buffer_capacity=n,
                n_disc_updates_per_round=5,
                venv=venv,
                gen_algo=learner,
                reward_net=reward_net,
                allow_variable_horizon=True
                )

            gail_trainer.train(n)
            trainer = learner

        # env = Monitor(env)
        callback_tracker_eval = CustomCallbackEval()
        env.reset()
        rewards, steps = evaluate_policy(trainer, env, int(timesteps/max_episode_steps), deterministic=False, return_episode_rewards=True, callback=callback_tracker_eval.callback_eval)
        
        episodes_list = []
        tsteps = 0
        for e in range(len(rewards)):
            tsteps += steps[e]
            done = 'False'
            if steps[e] < max_episode_steps:
                done = 'True'
            
            episode_essay = {
                            "serie": str(alg), 
                            "timesteps": str(tsteps),
                            "total_timesteps": str(timesteps),
                            "episode": str(e+1),
                            "last_step": str(steps[e]),
                            "reward": str(rewards[e]),
                            "done": str(done),
                            "actions": str(callback_tracker_eval.actions_list[e]),
                            "successes": callback_tracker_eval.n_success_episode_list[e],
                            "connection_errors": callback_tracker_eval.n_connection_error_episode_list[e],
                            "permission_errors": callback_tracker_eval.n_permission_error_episode_list[e]
                        }

            episodes_list.append(episode_essay)

        return episodes_list
      
    # read yaml scenery from nasim
    def load_nasim_scenery(self, scenery_name):
        # Open the file and load the file
        scenery_name = scenery_name.replace('NASIM:','')
        scenery_file = os.path.join('benchmark',f'{scenery_name}.yaml')
        with open(scenery_file) as f:
            data = yaml.safe_load(f)
            
            # get info
            os_list = data['os']
            firewall = data['firewall']
            sensitive_hosts = data['sensitive_hosts']
            exploits = data['exploits']
            privilege_escalation = data['privilege_escalation']
            host_configurations_list = data['host_configurations']
            action_space_size = len(host_configurations_list.keys()) * (len(exploits) + len(privilege_escalation) + 4)
            max_episode_steps = data['step_limit']

            # get switches
            topology_matrix = data['topology']
            n_switches = len(topology_matrix)
            switches_tuple_list = []
            for sw in range(0, n_switches):
                if sw == 0:
                 switches_tuple_list.append((f'switch-{sw}', f'Internet', 'switch', 'wks'))
                else:
                 switches_tuple_list.append((f'switch-{sw}', f'Subnet-{sw}', 'switch', 'srv'))

            # get firewalls
            firewalls_tuple_list = []
            subnets_list = data['subnets']
            n_firewalls = len(subnets_list)
            '''
            for fw in range(1, n_firewalls+1):
                 firewalls_tuple_list.append((f'firewall-{fw}', f'Firewall-{fw}', 'firewall', 'net'))
            '''

            # get hosts
            edges_tuple_list = [('kali-attacker', 'switch-0')]
            
            hosts_tuple_list = []
            for i, (k, v) in enumerate(host_configurations_list.items()):
                services = v['services']
                if 'lin' in v['os'].lower():
                    style = 'victim-linux'
                    if k in sensitive_hosts.keys():
                        style = 'victim-linux-sensive'
                elif 'win' in v['os'].lower():
                    style = 'victim-windows'
                    if k in sensitive_hosts.keys():
                        style = 'victim-windows-sensive'
                h = tuple(map(int, k.replace('(', '').replace(')', '').split(', ')))
                hosts_tuple_list.append((f'host-{str(h[0])+"-"+str(h[1])}', f'Host-{str(h[0])+"-"+str(h[1])+" "+str(services)}', style, 'srv'))
                edges_tuple_list.append((f'host-{str(h[0])+"-"+str(h[1])}', f'switch-{h[0]}'))
            
            # set nasim scenery zones
            scenery_nasim_zones = [('kali-attacker', 'Attacker', 'attacker', 'wks')] + switches_tuple_list + firewalls_tuple_list + hosts_tuple_list
            
            # get edges
            sw_pos = 0
            f = firewall.keys()
            access_style = []                                                
            for sw_and_fw in topology_matrix:
                for fw_pos in range(0, n_firewalls+1):
                    if sw_and_fw[fw_pos] == 1 and fw_pos > 0:
                        # edges_tuple_list.append((f'switch-{sw_pos}', f'firewall-{fw_pos}'))
                        if sw_pos != fw_pos:
                           edges_tuple_list.append((f'switch-{sw_pos}', f'switch-{fw_pos}'))
                        if f'({sw_pos}, {fw_pos})' in f:
                            allows = firewall[f'({sw_pos}, {fw_pos})']
                            if allows == []:
                                line_color = '#E34234'
                                label = ''
                            else:
                                line_color = 'lightgreen'
                                label = f'{allows}'
                            access_style.append(
                                {
                                    'selector': f'#switch-{sw_pos}switch-{fw_pos}',
                                    'style': {
                                        'curve-style': 'bezier',
                                        'label': f'{label}',
                                        'line-color': f'{line_color}',
                                        'font-size': 6,
                                        'line-style': 'dotted',
                                        'line': 0.5,
                                        'target-arrow-shape': 'triangle',
                                        'target-arrow-color': f'{line_color}',
                                    }
                                })

                sw_pos += 1


        return os_list, scenery_nasim_zones, edges_tuple_list, action_space_size, access_style, max_episode_steps

    # Sample Method
    def create_scenery_graph(self, scenery, images_list, scenery_size):

        # for os list RLI2 anable for change, nasim disabled for change    
        os_list = IMAGES_LIST
        images_checklist_options = [{"label": i ,"value": i, "disabled": False} for i in os_list]
        action_space_size = 0
        access_style = []


        if 'nasim' in scenery.lower():

            images_list, scenery_nasim_zones, edges_tuple_list, action_space_size, access_style, max_episode_steps = self.load_nasim_scenery(scenery)
            images_checklist_options = [{"label": i ,"value": i, "disabled": True} for i in images_list]

            zones = copy.copy(scenery_nasim_zones)            


        # infrastructure
        nodes_tuples_list = zones   

        # servers networks

        if 'nasim' not in scenery.lower():
            servers_networks = []
            for id_, label, style, connector in zones:
                if 'switch' in id_.lower() and connector == 'srv' and 'user' not in label.lower():
                    servers_networks.append(id_)

            srv_counter = 1
            for s in range(scenery_size):
                for n in servers_networks:
                    for i in images_list:

                        if 'lin' in i.lower():
                            nodes_tuples_list.append((f'srv-{srv_counter}', f'srv-{srv_counter}', 'victim-linux', 'switch'))
                        elif 'win' in i.lower():
                            nodes_tuples_list.append((f'srv-{srv_counter}', f'srv-{srv_counter}', 'victim-windows', 'switch'))
                        
                        edges_tuple_list.append((f'srv-{srv_counter}', n))
                        srv_counter += 1
            
            # Users networks          
            users_networks = []
            for id_, label, style, connector in zones:
                if 'switch' in id_.lower() and connector == 'wks' and 'user' in label.lower():
                    users_networks.append(id_)

            wks_counter = 1         
            for s in range(scenery_size):
                for n in users_networks:
                    for i in images_list:

                        # Workstations                    
                        if 'lin' in i.lower():
                            nodes_tuples_list.append((f'wks-{wks_counter}', f'wks-{wks_counter}', 'victim-linux', 'switch'))
                        if 'win' in i.lower():
                            nodes_tuples_list.append((f'wks-{wks_counter}', f'wks-{wks_counter}', 'victim-windows', 'switch'))
            
                        edges_tuple_list.append((f'wks-{wks_counter}', n))
                        wks_counter += 1
        
        nodes = [
                    {
                        'data': {'id': id_, 'label': label},
                        'classes': style,
                    } for id_, label, style, conector in nodes_tuples_list
                ]
    
        edges = [
                    {'data': {'id': source+target,'source': source, 'target': target}}
                    for source, target in edges_tuple_list
                ]
        
        return nodes, edges, images_checklist_options, action_space_size, access_style, max_episode_steps


class CustomCallbackEval():

    def __init__(self):
        # Create folder if needed
        self.episode = 0
        self.reward_episode = 0
        self.steps_episode = 0
        self.list_rewards_episodes = []
        self.list_steps_episodes = []
        self.done = False        
        self.n_success_episode = 0
        self.n_connection_error_episode = 0
        self.n_permission_error_episode = 0
        
        self.n_success_episode_list = []
        self.n_connection_error_episode_list = []
        self.n_permission_error_episode_list = []

        self.list_results = []

        self.actions_list = []
        self.episode_actions_list = []

    def callback_eval(self, locals, globals):
        # print(locals.keys())
        # print(locals["current_lengths"])
        '''
        if locals['episode_counts'][0] > self.episode and locals["current_lengths"] == 1:
            self.episode = locals['episode_counts'][0]
            self.actions_list.append(self.espisode_actions_list)
            self.espisode_actions_list = []
            self.done = False
        '''
        # print(locals['done'],locals['episode_counts'][0])
        # if str(locals['done']) == "False":
        self.episode_actions_list.append(locals['actions'][0])

        if locals['infos'][0]['success'] == True:
            self.n_success_episode += 1
        if locals['infos'][0]['connection_error'] == True:
            self.n_connection_error_episode += 1
        if locals['infos'][0]['permission_error'] == True:
            self.n_permission_error_episode += 1

        if str(locals['done']) == 'True':
            self.actions_list.append(self.episode_actions_list)
            self.episode_actions_list = []

            self.n_success_episode_list.append(self.n_success_episode)
            self.n_connection_error_episode_list.append(self.n_connection_error_episode)
            self.n_permission_error_episode_list.append(self.n_permission_error_episode)

            self.n_success_episode = 0
            self.n_connection_error_episode = 0
            self.n_permission_error_episode = 0

class CustomCallback(BaseCallback):
    def __init__(self, verbose=0):
        super(CustomCallback, self).__init__(verbose)
        # Create folder if needed
        self.episode = 0
        self.reward_episode = 0
        self.steps_episode = 0
        self.n_success_episode = 0
        self.n_connection_error_episode = 0
        self.n_permission_error_episode = 0
        
        self.list_rewards_episodes = []
        self.list_steps_episodes = []
        self.done = False        
        self.list_results = []

        self.actions_list = []

    def _on_training_start(self) -> None:
        """
        This method is called before the first rollout starts.
        """
        pass

    def _on_rollout_start(self) -> None:
        """
        A rollout is the collection of environment interaction
        using the current policy.
        This event is triggered before collecting new samples.
        """
        self.model.env.reset()
        self.episode += 1
        self.reward_episode = 0
        self.steps_episode = 0
        self.done = False
        self.n_success_episode = 0
        self.n_connection_error_episode = 0
        self.n_permission_error_episode = 0
        
        self.actions_list = []

    def _on_step(self) -> bool:
        """
        This method will be called by the model after each call to `env.step()`.

        For child callback (of an `EventCallback`), this will be called
        when the event is triggered.

        :return: (bool) If the callback returns False, training is aborted early.
        """

        if self.done == False: 
            self.reward_episode += self.locals['rewards'][0]
            self.steps_episode += 1
            self.done = self.locals['dones'][0]
            if self.locals['infos'][0]['success'] == True:
                self.n_success_episode += 1
            if self.locals['infos'][0]['connection_error'] == True:
                self.n_connection_error_episode += 1
            if self.locals['infos'][0]['permission_error'] == True:
                self.n_permission_error_episode += 1
                    
            # self.actions_list.append({"action": self.locals['actions'][0], "description": self.model.action_space.__dict__['actions'][self.locals['actions'][0]]})
            self.actions_list.append(self.locals['actions'][0])
        return True

    def _on_rollout_end(self) -> None:
        """
        This event is triggered before updating the policy.
        """

        self.list_rewards_episodes.append(self.reward_episode)
        self.list_steps_episodes.append(self.steps_episode)
        
        print('#'*50)
        print(f'serie:', self.locals['tb_log_name'])
        print(f'timesteps: {self.num_timesteps}/{self.locals["total_timesteps"]}')
        print("episode", self.episode)
        print('step:', self.steps_episode)        
        print('reward episode:', self.reward_episode)
        print('done:', self.done)
        print('mean steps:', mean(self.list_steps_episodes) )
        print('mean rewards:', mean(self.list_rewards_episodes) )
        print('successes:', self.n_success_episode)
        print('connection_errors:', self.n_connection_error_episode)
        print('permission_errors:', self.n_permission_error_episode)
        print('actions:', len(self.actions_list))
        
        episode_essay = {
            "serie": self.locals['tb_log_name'], 
            "timesteps": self.num_timesteps,
            "total_timesteps": self.locals["total_timesteps"],
            "episode": self.episode,
            "last_step": self.steps_episode,
            "reward": self.reward_episode,
            "done": str(self.done),
            "actions": str(self.actions_list),
            "successes": self.n_success_episode,
            "connection_errors": self.n_connection_error_episode,
            "permission_errors": self.n_permission_error_episode
        
        }

        self.list_results.append(episode_essay) 

    def _on_training_end(self) -> None:
        """
        This event is triggered before exiting the `learn()` method.
        """
        '''
        path = "results"
        filename = "results.json"
        results_path = os.path.join(path,filename)
    
        # Check if file exists
        if os.path.exists(results_path) is False:
            with open(results_path, "w") as write_file:
                json.dump(self.list_results, write_file)
        else:
            # Read JSON file
            with open(results_path) as write_file:
                listObj = json.load(write_file)
                listObj.append(self.list_results)

            with open(results_path, "w") as write_file:
                json.dump(listObj, write_file)
        '''
        pass       
