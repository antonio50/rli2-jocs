# Format = {id, label, style, type_connector}
SCENERY_A_ZONES = [
                ('kali-attacker', 'Attacker', 'attacker', 'wks'),
                ('firewall', 'Firewall', 'firewall', 'net'),
                ('switch-engineering', 'Engineering', 'switch', 'srv'),
                ('switch-corporate', 'Corporate', 'switch', 'srv'),
                ('switch-users', 'Users', 'switch', 'wks')
            ]

SCENERY_B_ZONES = [
                ('kali-attacker', 'Attacker', 'attacker', 'wks'),
                ('firewall-edge', 'Firewall-edge', 'firewall', 'net'),
                ('firewall-corporate', 'Firewall-corporate', 'firewall', 'net'),
                ('firewall-engineering', 'Firewall-egineering', 'firewall', 'net'),
                ('switch-engineering', 'Engineering', 'switch', 'srv'),
                ('switch-engineering-remote', 'Engineering-remote', 'switch', 'srv'),
                ('switch-corporate', 'Corporate', 'switch', 'srv'),
                ('switch-corporate-remote', 'Corporate-remote', 'switch', 'srv'),
                ('switch-users', 'Users', 'switch', 'wks'),
                ('switch-users-remote', 'Users-remote', 'switch', 'wks'),
                ('switch-dmz', 'DMZ', 'switch', 'srv'),
            ]

SCENERY_C_ZONES = [
                ('kali-attacker', 'Attacker', 'attacker', 'wks'),
                ('firewall-edge', 'Firewall-edge', 'firewall', 'net'),
                ('firewall-corporate', 'Firewall-corporate', 'firewall', 'net'),
                ('firewall-engineering', 'Firewall-egineering', 'firewall', 'net'),
                ('switch-engineering', 'Engineering', 'switch', 'srv'),
                ('switch-engineering-remote-1', 'Engineering-remote', 'switch', 'srv'),
                ('switch-engineering-remote-2', 'Engineering-remote', 'switch', 'srv'),
                ('switch-engineering-remote-3', 'Engineering-remote', 'switch', 'srv'),
                ('switch-corporate', 'Corporate', 'switch', 'srv'),
                ('switch-corporate-remote-1', 'Corporate-remote', 'switch', 'srv'),
                ('switch-corporate-remote-2', 'Corporate-remote', 'switch', 'srv'),
                ('switch-corporate-remote-3', 'Corporate-remote', 'switch', 'srv'),
                ('switch-users', 'Users', 'switch', 'wks'),
                ('switch-users-remote', 'Users-remote', 'switch', 'wks'),
                ('switch-dmz', 'DMZ', 'switch', 'srv'),
                ('switch-extranet', 'EXTRANET', 'switch', 'net'),
                ('firewall-extranet-1', 'Firewall-extranet-1', 'firewall', 'net'),
                ('firewall-extranet-2', 'Firewall-extranet-1', 'firewall', 'net'),
                ('firewall-extranet-3', 'Firewall-extranet-1', 'firewall', 'net'),
                ('switch-extranet-1', 'EXTRANET-1', 'switch', 'net'),
                ('switch-extranet-2', 'EXTRANET-2', 'switch', 'net'),
                ('switch-extranet-3', 'EXTRANET-3', 'switch', 'net'),
            ]

# SCENERY_LIST = ["RLI2:Enterprise A", "RLI2:Enterprise B", "RLI2:Enterprise C"]
SCENERY_LIST = []

IMAGES_LIST = ["linux", "windows"]

SERIES_LIST = [ "DQN",
                "DQN+TL",
                "DQN+BC",
                "DQN+DAgger",
                "DQN+GAIL",
                # "QRDQN",
                # "QRDQN+TL",
                # "QRDQN+BC",
                # "QRDQN+DAgger",
                # "QRDQN+GAIL",
                # "QRDQN+SneakyAdvisor",
                "A2C",
                "A2C+TL",
                "A2C+BC",
                "A2C+DAgger",
                "A2C+GAIL",
                "TRPO",
                "TRPO+TL",
                "TRPO+BC",
                "TRPO+DAgger",
                "TRPO+GAIL",
                "PPO",
                "PPO+TL",
                "PPO+BC",
                "PPO+DAgger",
                "PPO+GAIL",
                "RecurrentPPO",
                "RecurrentPPO+TL",
                "RecurrentPPO+BC",
                "RecurrentPPO+DAgger",
                # "RecurrentPPO+GAIL"                
              ]

DEFAULT_STYLESHEET = [
            # Group selectors
            {
                'selector': 'node',
                'style': {
                    'content': 'data(label)',
                    'font-size': '8pt',
                }
            },

            # Class selectors
            {
                'selector': '.attacker',
                'style': {
                    'background-opacity': '0',
                    # 'background-color': 'red',
                    # 'line-color': 'red',
                    'shape': 'circle',
                    'background-fit': 'cover',
                    'background-image': 'assets/images/RLI-icon-alpha.png'
                    
                }
            },
            {
                'selector': '.victim-linux',
                'style': {
                    'shape': 'circle',
                    'background-opacity': '0',
                    # 'background-color': 'silver',
                    # 'line-color': 'silver',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/linux-bw.png'
                }
            },
            {
                'selector': '.victim-windows',
                'style': {
                    'shape': 'square',
                    'background-opacity': '0',
                    # 'background-color': 'silver',
                    # 'line-color': 'silver',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/win2k8-bw.png'                    
                }
            },
            {
                'selector': '.victim-linux-sensive',
                'style': {
                    'shape': 'circle',
                    'background-opacity': '0',
                    # 'background-color': 'silver',
                    # 'line-color': 'silver',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/linux.png'
                }
            },
            {
                'selector': '.victim-windows-sensive',
                'style': {
                    'shape': 'square',
                    'background-opacity': '0',
                    # 'background-color': 'silver',
                    # 'line-color': 'silver',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/win2k8.png'                    
                }
            },
            {
                'selector': '.switch',
                'style': {
                    'shape': 'square',
                    'background-opacity': '0',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/firewall.png'
                }
            },
            {
                'selector': '.firewall',
                'style': {
                    'shape': 'square',
                    'background-opacity': '0',
                    'background-fit': 'contain',
                    'background-image': 'assets/images/firewall.png'
                }
            },
            {
                'selector': '#switch-1firewall-1, #switch-2firewall-1',
                'style': {
                    'curve-style': 'haystack',
                    'label': 'ssh,http',
                    'line-color': 'green',
                    'font-size': 6,
                    'line-style': 'dotted',
                    'line': 1,
                    # 'target-arrow-shape': 'triangle',

                }
            },
]

TAB_SIZE_STYLE = {'width': '100%', 'height': '711px'}
FIG_SIZE_STYLE = {'width': '100%', 'height': '720px'}
SCATTER_SIZE_STYLE = {'width': '100%', 'height': '460px'}

GLYPHS_RANGES_DECIMAL = [
    "65-90",
    "97-122",
    "913-929",
    "931-999",
    "1000-1154",
    "1162-1327",
    "1329-1366",
    "1377-1415",
    "1488-1514",
    "1568-1610",
    "1649-1747",
    "1808-1839",
    "1869-1919",
    "1984-2035",
    "2046-2069",
    "2112-2139",
    "2304-2384",
    "2390-2444",
    "2451-2472",
    "2579-2600",
    "3346-3396",
    "3482-3505",
    "3585-3639",
    "3840-3948",
    "4096-4293",
    "4304-4446",
    "4449-4680"
]

GLYPHS = []
for r in GLYPHS_RANGES_DECIMAL:
    for g in range(int(r.split("-")[0]),int(r.split("-")[1])):
        GLYPHS.append(chr(g))