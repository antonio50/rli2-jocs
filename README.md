[![Watch the video](https://gitlab.com/cybercrafter/rli2-jocs/-/raw/main/images/rli2youtube.png?ref_type=heads)](https://youtu.be/azw0nwm_z8c)

# Red Lord Invader by Reinforcement Learning Interaction (RLI<sup>2</sup>)

RLI<sup>2</sup>Lab is a playground for experiments with Reinforcement Learning and attack simulators, such as NASIM.  The purpose of RLI<sup>2</sup> is put in practice new concepts for autonomous ai-driven Kill chains throught RL interaction.   



# Abstract

Organizations are vulnerable to cyber attacks as they rely on computer networks and the internet for communication and data storage. While Reinforcement Learning (RL) is a widely used strategy to simulate and learn from these attacks, RL-guided offensives against unknown scenarios often lead to early exposure due to low stealth resulting from mistakes during the training phase. To address this issue, this work evaluates if the use of Knowledge Transfer Techniques (KTT), such as Transfer Learning and Imitation Learning, reduces the probability of early exposure by smoothing mistakes during training. This study developed a laboratory platform and a method to compare RL-based cyber attacks using KTT for unknown scenarios. The experiments simulated 2 unknown scenarios using 4 traditional RL algorithms and 4 KTT. In the results, although some algorithms using KTT obtained superior results, they were not so significant for stealth during the initial epochs of training. Nevertheless, experiments also revealed that throughout the entire learning cycle, Trust Region Policy Optimization (TRPO) is a promising algorithm for conducting cyber offensives based on Reinforcement Learning.

# Journal of Computer Security 

Paper submited to JOCS (https://www.iospress.com/catalog/journals/journal-of-computer-security)

# Datasets

The experiments rollouts and results can be checked in datasets folder.

# Summary
 
1. Scenario: Scenery Selector (NASIM)
2. Essays: Select Base algorithms for Essays (DQN, QRDQN, A2C, TRPO, PPO and RecurrentPPO)
4. Support Transfer Learning and Imitation from generic sceneries (TL, BC, DAgger and GAIL)

# Setup

```
python3 -m venv venv
source /venv/bin/activate
pip install -r requirements.txt
cp benchmark venv/lib/python3.10/site-packages/nasim/scenarios/benchmark
python rli2lab.py
```

# BUG FIXES: 

**rli/venv/lib/python3.10/site-packages/nasim/envs/action.py**
```
def get_action(self, action_idx):
    action_idx = int(action_idx) <<<<<<<<<< INSERT THIS LINE TO USE GYM (JUST ON ENV)
```

**rli/venv/lib/python3.10/site-packages/torch/distributions/distribution.py**
```
value = getattr(self, param)
value[torch.isnan(value)] = 0  <<<<<<<<<< INSERT THIS LINE 54 IN PYTORCH
valid = constraint.check(value)
```             

