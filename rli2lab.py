##################################################################
# 
# <Lab Web Interface>
# 
# RLI2Lab - Red Lor Invader for Reinforcement Learning Interaction
# by Antonio Horta --- 2023
# ajhorta@cybercrafter.com.br
#
##################################################################

from dash import Dash, dcc, html, dash_table, Input, Output, callback
import plotly.express as px
import dash_bootstrap_components as dbc
from dash_bootstrap_templates import ThemeChangerAIO, template_from_url, ThemeSwitchAIO
import dash_cytoscape as cyto
import dash_daq as daq
import dash_bio as dashbio

from constants import *
from rli2 import RLI2
import copy
import multiprocessing
import datetime
import os
import shutil
import pandas as pd
import numpy as np
import json

from dash_bootstrap_templates import load_figure_template

columns_list = ["serie",
                "timesteps",
                "total_timesteps",
                "episode",
                "last_step",
                "reward",
                "done",
                "actions",
                "steps_MA10",
                "rewards_MA10",
                "successes",
                "connection_errors",
                "permission_errors",
                "successes_rate_MA10",
                "connection_errors_rate_MA10",
                "permission_errors_rate_MA10",
                "successes_rate_std",
                "connection_errors_rate_std",
                "permission_errors_rate_std"]

df0 = pd.DataFrame(np.zeros((1, len(columns_list))), columns=columns_list)

# stylesheet with the .dbc class
dbc_css = "assets/css/dbc.min.css"
app = Dash(__name__, external_stylesheets=[dbc.themes.ZEPHYR, dbc_css])

##########################
# RLI2 web components
##########################

# Page header
header = dbc.Row([

                    dbc.Col(html.Img(src="assets/images/RLI-icon-alpha.png", width="55"), className="align-items-center mb-2"),
                    dbc.Col(html.H5("[ RLI\u00B2Lab ] Red Lord Invader with Reinforcement Learning Interaction"), className="col-7 text-left align-items-center"),
                    # dbc.Col(ThemeChangerAIO(aio_id="theme", radio_props={"value":dbc.themes.SIMPLEX}), className="col-2 float-right align-items-center mb-2"),
                    dbc.Col(ThemeSwitchAIO(aio_id="theme", themes=[dbc.themes.ZEPHYR, dbc.themes.DARKLY]), className="col-2 p-2 m-2"),

                ], className="bg-primary text-white p-0 mb-3 pt-2 justify-content-start align-items-center")

# Results Table
table = html.Div(
    dash_table.DataTable(
        id="results-table",
        columns=[{"name": i, "id": i, "deletable": True} for i in columns_list],
        data=df0.to_dict('records'),
        page_size=25,
        editable=False,
        cell_selectable=True,
        filter_action="native",
        sort_action="native",
        style_table={"overflowX": "auto"},
        row_selectable="multi",
        export_format="xlsx"
    ),
    className="dbc-row-selectable",
)

# Scenery Selector
rli2 = RLI2()
scenery_dropdown = html.Div(
    [
        dbc.Badge(
            "Scenery",
            color="light",
            # text_color="primary",
            className="border me-1 mb-2 w-100",
        ),
        dcc.Dropdown(
            rli2.get_scenery_list(),
            rli2.get_scenery_list()[0],
            id="scenery-dropdown",
            clearable=False
        ),
    ],
    className="mb-2",
)

# Operation System Selector: Images
images_checklist = html.Div(
    [
        dbc.Checklist(
            id="images-checklist",
            options=[{"label": i, "value": i, "disabled": False} for i in IMAGES_LIST],
            value=[IMAGES_LIST[1]],     
            switch=True,
        )            
    ], className="mb-2")   

# Scenery Size Selector: Small, Medium, Large
scenery_size_slider = html.Div(
    [
        # dbc.Label("Scenery size"),
        dcc.Slider(
            1,            
            17,
            step=4,
            id="scenery-size-slider",
            value=1,
            className="p-2",
            marks={1: 'small', 5: "",  9: 'medium', 13: "", 17: 'large'},
            disabled=False
            
        ),
    ], className="mb-4",
)


# Gauge to present host in scenery  
scenery_gauge = daq.Gauge(
    id="scenery-gauge",
    showCurrentValue=True,
    # digits=0,
    units="HOSTS",
    value=0,
    label='Scenery',
    max=400,
    min=0,
    size=150,
)

# Gauge to present host in scenery  
scenery_gauge_action_space_size = daq.Gauge(
    id="scenery-gauge-action-space-size",
    showCurrentValue=True,
    # digits=0,
    units="SIZE",
    value=0,
    label='Action Space',
    max=200,
    min=0,
    size=150,
)

# Select Agent/Algorithm
series_checklist = dbc.Row(
    [
        dbc.Badge(
            "Algorithms",
            color="light",
            # text_color="primary",
            className="border mx-1 mb-2",
        ),
        dbc.Col([
            dbc.Checklist(
                id="series-checklist",
                options=[{"label": i, "value": i} for i in SERIES_LIST],
                value=[],
                inline=True,
                switch=True)
        ], className="ml-1 pl-0 col-2")
    ], className="mb-4",
)

# Essay num Episodes Selector: 1000-1M
essay_episode_slider = dbc.Row(
    [
        dbc.Badge(
            "Episodes",
            color="light",
            # text_color="primary",
            className="border me-1 mb-2",
        ),
        dcc.Slider(
            1,            
            10_000,
            1,
            value=100,
            marks=None,
            tooltip={"placement": "bottom", "always_visible": True},
            id="essay-episodes-slider",
            className="p-2",
        ), 
    ], className="mb-4"
)

essay_spe_slider = dbc.Row(
    [
        dbc.Badge(
            "Steps per episode",
            color="light",
            # text_color="primary",
            className="border me-1 mb-2",
        ),
        dcc.Slider(
            10,            
            10_000,
            10,
            value=100,
            marks=None,
            tooltip={"placement": "bottom", "always_visible": True},
            id="essay-spe-slider",
            className="p-2",
            disabled=True,
        ), 
    ], className="mb-4"
)

# Gauge to present host in scenery  
essay_gauge = daq.Gauge(
    id="essay-gauge",
    showCurrentValue=True,
    # digits=0,
    units="STEPS",
    value=100_000,
    label='Timesteps',
    max=100_000_000,
    min=10_000,
)


load_essay_dropdown = html.Div(
    [
        dbc.Badge(
            "Load Essay",
            color="light",
            # text_color="primary",
            className="border me-1 mb-2 w-100",
        ),
        dcc.Dropdown(
            rli2.get_essay_list(),
            rli2.get_essay_list()[0],
            id="load-essay-dropdown",
            clearable=False,
        ),
    ],
    className="mb-2",
)


# Simulation button
attack_button = html.Div([
                            dbc.Badge(
                                        "<unknown>",
                                        color="light",
                                        # text_color="primary",
                                        className="border mx-0 p-2 mb-2 w-100",
                                        id="scenery-name"),
                            dbc.Button( "Attack!", 
                                        id="attack-button", 
                                        color="danger", 
                                        disabled=True, 
                                        className="p-4 m-0 w-100"), 
                            dcc.Interval( id='interval-component',
                                          interval=3*1000, # in milliseconds
                                          n_intervals=0,
                                          disabled=False)                           
                         ], className="mb-2")








# Group all components in frames
controls_scenery = dbc.Card([scenery_dropdown, images_checklist, scenery_size_slider], body=True)
dashboard_scenery = dbc.Card([scenery_gauge, scenery_gauge_action_space_size], body=True, className="mt-2 p-0")

controls_essay = dbc.Card([load_essay_dropdown, attack_button, series_checklist, essay_episode_slider, essay_spe_slider], body=True)
dashboard_essay = dbc.Card([essay_gauge], body=True, className="mt-2")

# Network Graph - Scenery
scenery_graph = html.Div([
                    cyto.Cytoscape(
                        id='scenery-graph',
                        # elements=None,
                        style=TAB_SIZE_STYLE,
                        layout={'name': 'cose'},
                        stylesheet=DEFAULT_STYLESHEET,
                        boxSelectionEnabled=True,
                        panningEnabled=True,        
                    )
                ])



scatter_chart_successes_figure = px.scatter(
        df0,
        x="last_step",
        y="successes",
        color="serie",
        title="First episode",
)
scatter_chart_connection_errors_figure = px.scatter(
        df0,
        x="last_step",
        y="connection_errors",
        color="serie",
        title="First episode",
)
scatter_chart_permission_errors_figure = px.scatter(
        df0,
        x="last_step",
        y="permission_errors",
        color="serie",
        title="First episode"
)


line_chart_steps_figure = px.line(
        df0,
        x="episode",
        y="last_step",
        color="serie",
        line_group="serie",
)

line_chart_score_figure = px.line(
        df0,
        x="episode",
        y="reward",
        color="serie",
        line_group="serie",
)

line_chart_stepsmm_figure = px.line(
        df0,
        x="episode",
        y="steps_MA10",
        color="serie",
        line_group="serie",
)

line_chart_scoremm_figure = px.line(
        df0,
        x="episode",
        y="rewards_MA10",
        color="serie",
        line_group="serie",
)

line_chart_successes_figure = px.line(
        df0,
        x="episode",
        y="successes",
        color="serie",
        line_group="serie",
)
line_chart_connection_errors_figure = px.line(
        df0,
        x="episode",
        y="connection_errors",
        color="serie",
        line_group="serie",
)
line_chart_permission_errors_figure = px.line(
        df0,
        x="episode",
        y="permission_errors",
        color="serie",
        line_group="serie",
)
line_chart_successes_ma_figure = px.line(
        df0,
        x="episode",
        y="successes_rate_MA10",
        color="serie",
        line_group="serie",
)
line_chart_connection_errors_ma_figure = px.line(
        df0,
        x="episode",
        y="connection_errors_rate_MA10",
        color="serie",
        line_group="serie",
)
line_chart_permission_errors_ma_figure = px.line(
        df0,
        x="episode",
        y="permission_errors_rate_MA10",
        color="serie",
        line_group="serie",
)
bar_chart_successes_ma_figure = px.histogram(
        df0,
        x="serie",
        y="successes_rate_MA10",
        color="serie",
        barmode='group',
        histfunc='avg',
        title="Average from episodes"
)
bar_chart_connection_errors_ma_figure = px.histogram(
        df0,
        x="serie",
        y="connection_errors_rate_MA10",
        color="serie",
        barmode='group',
        histfunc='avg',
        title="Average from episodes"
)
bar_chart_permission_errors_ma_figure = px.histogram(
        df0,
        x="serie",
        y="permission_errors_rate_MA10",
        color="serie",
        barmode='group',
        histfunc='avg',
        title="Average from episodes"
)
bar_chart_successes_std_figure = px.histogram(
        df0,
        x="serie",
        y="successes_rate_std",
        color="serie",
        barmode='group',
        title="Standard Deviation from episodes"
)
bar_chart_connection_errors_std_figure = px.histogram(
        df0,
        x="serie",
        y="connection_errors_rate_std",
        color="serie",
        barmode='group',
        title="Standard Deviation from episodes"
)
bar_chart_permission_errors_std_figure = px.histogram(
        df0,
        x="serie",
        y="permission_errors_rate_std",
        color="serie",
        barmode='group',
        title="Standard Deviation from episodes"
)


scatter_chart_successes = dcc.Graph(id="scatter-chart-successes", style=SCATTER_SIZE_STYLE, figure=scatter_chart_successes_figure)
scatter_chart_connection_errors = dcc.Graph(id="scatter-chart-connection-errors", style=SCATTER_SIZE_STYLE, figure=scatter_chart_connection_errors_figure)
scatter_chart_permission_errors = dcc.Graph(id="scatter-chart-permission-errors", style=SCATTER_SIZE_STYLE, figure=scatter_chart_permission_errors_figure)
line_chart_steps = dcc.Graph(id="line-chart-steps", style=FIG_SIZE_STYLE, figure=line_chart_steps_figure)
line_chart_score = dcc.Graph(id="line-chart-score", style=FIG_SIZE_STYLE, figure=line_chart_score_figure)
line_chart_stepsmm = dcc.Graph(id="line-chart-stepsmm", style=FIG_SIZE_STYLE, figure=line_chart_stepsmm_figure)
line_chart_scoremm = dcc.Graph(id="line-chart-scoremm", style=FIG_SIZE_STYLE, figure=line_chart_scoremm_figure)
line_chart_successes = dcc.Graph(id="line-chart-successes", style=FIG_SIZE_STYLE, figure=line_chart_successes_figure)
line_chart_connection_errors = dcc.Graph(id="line-chart-connection-errors", style=FIG_SIZE_STYLE, figure=line_chart_connection_errors_figure)
line_chart_permission_errors = dcc.Graph(id="line-chart-permission-errors", style=FIG_SIZE_STYLE, figure=line_chart_permission_errors_figure)
line_chart_successes_ma = dcc.Graph(id="line-chart-successes-ma", style=FIG_SIZE_STYLE, figure=line_chart_successes_ma_figure)
line_chart_connection_errors_ma = dcc.Graph(id="line-chart-connection-errors-ma", style=FIG_SIZE_STYLE, figure=line_chart_connection_errors_ma_figure)
line_chart_permission_errors_ma = dcc.Graph(id="line-chart-permission-errors-ma", style=FIG_SIZE_STYLE, figure=line_chart_permission_errors_ma_figure)
bar_chart_successes_ma = dcc.Graph(id="bar-chart-successes-ma", style=FIG_SIZE_STYLE, figure=bar_chart_successes_ma_figure)
bar_chart_connection_errors_ma = dcc.Graph(id="bar-chart-connection-errors-ma", style=FIG_SIZE_STYLE, figure=bar_chart_connection_errors_ma_figure)
bar_chart_permission_errors_ma = dcc.Graph(id="bar-chart-permission-errors-ma", style=FIG_SIZE_STYLE, figure=bar_chart_permission_errors_ma_figure)
bar_chart_successes_std = dcc.Graph(id="bar-chart-successes-std", style=FIG_SIZE_STYLE, figure=bar_chart_successes_std_figure)
bar_chart_connection_errors_std = dcc.Graph(id="bar-chart-connection-errors-std", style=FIG_SIZE_STYLE, figure=bar_chart_connection_errors_std_figure)
bar_chart_permission_errors_std = dcc.Graph(id="bar-chart-permission-errors-std", style=FIG_SIZE_STYLE, figure=bar_chart_permission_errors_std_figure)


tab1 = dbc.Tab([
            dbc.Row([
                dbc.Col([
                    dbc.Row([
                        controls_scenery, dashboard_scenery
                    ])
                ], width=3),
                dbc.Col([
                        scenery_graph                    
                ], width=9),
            ])
        ], label="Scenery")

tab2 = dbc.Tab([
            dbc.Row([
                dbc.Col([
                    dbc.Row([
                        controls_essay, dashboard_essay
                    ])
                ], width=3),
                dbc.Col([
                    dbc.Row([
                        dbc.Col([
                            scatter_chart_successes,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            scatter_chart_connection_errors,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            scatter_chart_permission_errors,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_steps,
                        ], className="col-6 p-1 m-0"),
                        dbc.Col([
                            line_chart_score,
                        ], className="col-6 p-1 m-0"),
                        dbc.Col([
                            line_chart_stepsmm,
                        ], className="col-6 p-1 m-0"),
                        dbc.Col([
                            line_chart_scoremm,
                        ], className="col-6 p-1 m-0"),
                        dbc.Col([
                            line_chart_successes,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_connection_errors,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_permission_errors,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_successes_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_connection_errors_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            line_chart_permission_errors_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_successes_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_connection_errors_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_permission_errors_ma,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_successes_std,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_connection_errors_std,
                        ], className="col-4 p-1 m-0"),
                        dbc.Col([
                            bar_chart_permission_errors_std,
                        ], className="col-4 p-1 m-0"),
                        table
                    ])
                ], width=9),
            ])
        ], label="Essay", className="p-4", style=TAB_SIZE_STYLE)


# tabs = dbc.Card(dbc.Tabs([tab1, tab2]))
tabs = dbc.Tabs([tab1, tab2], className="m-2")

# join all frames in a web container
app.layout = dbc.Container(
    [
        dcc.Store(id='attack-process'),
        header,   
        dbc.Row(
            [
               tabs,
            ], className="m-1 p-0"),
    ], fluid=True, className="dbc")

#################################
# RLI2LAB - Web application I/O
#################################
@callback(
    Output("scenery-graph", "elements"),
    Output("images-checklist", "value"),
    Output("images-checklist", "options"),
    Output("scenery-size-slider","disabled"),
    Output("scenery-gauge", "value"),
    Output("scenery-name", "children"),
    Output("scenery-gauge-action-space-size", "value"),
    Output("scenery-graph", "stylesheet"),    
    Output("essay-spe-slider","value"),
    Input("scenery-dropdown", "value"),
    Input("images-checklist", "value"),
    Input("scenery-size-slider","value"),    
)
def update_scenery(scenery_dropdown_value, images_checklist_value, scenery_size_slider_value):

    rli2 = RLI2()
    nodes, edges, images_checklist_options, scenery_gauge_action_space_size_value, access_style, essay_spe_slider_value  = rli2.create_scenery_graph(scenery=scenery_dropdown_value, images_list=images_checklist_value, scenery_size=scenery_size_slider_value)
    scenery_dropdown_elements = nodes + edges

    scenery_graph_stylesheet = DEFAULT_STYLESHEET + access_style

    scenery_name_children = scenery_dropdown_value.split(":")[1]
    
    hosts = 0
    for n in nodes:
        if 'victim' in n['classes']:
            hosts += 1

    scenery_gauge_value = hosts

    scenery_size_slider_disabled = False
    if len(images_checklist_value) == 0:
        images_checklist_value = IMAGES_LIST
    elif images_checklist_options[0]['disabled'] is True:
        images_checklist_value = []
        for i in images_checklist_options: 
            images_checklist_value.append(i['label'])
            scenery_size_slider_disabled = True
         

    return scenery_dropdown_elements, \
           images_checklist_value, \
           images_checklist_options, \
           scenery_size_slider_disabled, \
           scenery_gauge_value, \
           scenery_name_children, \
           scenery_gauge_action_space_size_value, \
           scenery_graph_stylesheet, \
           essay_spe_slider_value

@callback(
    Output("essay-gauge", "value"),
    Output("attack-button", "disabled"),
    Output("attack-button", "n_clicks"),
    Output("attack-button", "children"),
    Output("results-table", "data"),
    Output("scatter-chart-successes","figure"),
    Output("scatter-chart-connection-errors","figure"),
    Output("scatter-chart-permission-errors","figure"),
    Output("line-chart-steps","figure"),
    Output("line-chart-score","figure"),
    Output("line-chart-stepsmm","figure"),
    Output("line-chart-scoremm","figure"),
    Output("line-chart-successes","figure"),
    Output("line-chart-connection-errors","figure"),
    Output("line-chart-permission-errors","figure"),
    Output("line-chart-successes-ma","figure"),
    Output("line-chart-connection-errors-ma","figure"),
    Output("line-chart-permission-errors-ma","figure"),
    Output("bar-chart-successes-ma","figure"),
    Output("bar-chart-connection-errors-ma","figure"),
    Output("bar-chart-permission-errors-ma","figure"),
    Output("bar-chart-successes-std","figure"),
    Output("bar-chart-connection-errors-std","figure"),
    Output("bar-chart-permission-errors-std","figure"),
    Output("interval-component","disabled"),
    Output("load-essay-dropdown","value"),
    Input("essay-episodes-slider","value"),
    Input("essay-spe-slider","value"),
    Input("series-checklist","value"),
    Input("attack-button","n_clicks"),
    Input("attack-button","children"),
    Input("interval-component","n_intervals"),
    Input("interval-component","disabled"),
    Input("results-table", "data"),
    Input("scatter-chart-successes","figure"),
    Input("scatter-chart-connection-errors","figure"),
    Input("scatter-chart-permission-errors","figure"),
    Input("line-chart-steps","figure"),
    Input("line-chart-score","figure"),
    Input("line-chart-stepsmm","figure"),
    Input("line-chart-scoremm","figure"),
    Input("line-chart-successes","figure"),
    Input("line-chart-connection-errors","figure"),
    Input("line-chart-permission-errors","figure"),
    Input("line-chart-successes-ma","figure"),
    Input("line-chart-connection-errors-ma","figure"),
    Input("line-chart-permission-errors-ma","figure"),
    Input("bar-chart-successes-ma","figure"),
    Input("bar-chart-connection-errors-ma","figure"),
    Input("bar-chart-permission-errors-ma","figure"),
    Input("bar-chart-successes-std","figure"),
    Input("bar-chart-connection-errors-std","figure"),
    Input("bar-chart-permission-errors-std","figure"),
    Input("scenery-name","children"),
    Input("load-essay-dropdown","value"),
    
)
def update_essay(essay_episodes_slider, 
                 essay_spe_slider, 
                 series_checklist_value, 
                 attack_button_n_clicks, 
                 attack_button_children, 
                 interval_component_n_intervals, 
                 interval_component_disabled, 
                 results_table_data, 
                 scatter_chart_successes_figure, 
                 scatter_chart_connection_errors_figure, 
                 scatter_chart_permission_errors_figure, 
                 line_chart_steps_figure, 
                 line_chart_score_figure, 
                 line_chart_stepsmm_figure, 
                 line_chart_scoremm_figure, 
                 line_chart_successes_figure, 
                 line_chart_connection_errors_figure, 
                 line_chart_permission_errors_figure, 
                 line_chart_successes_ma_figure, 
                 line_chart_connection_errors_ma_figure, 
                 line_chart_permission_errors_ma_figure, 
                 bar_chart_successes_ma_figure, 
                 bar_chart_connection_errors_ma_figure, 
                 bar_chart_permission_errors_ma_figure, 
                 bar_chart_successes_std_figure, 
                 bar_chart_connection_errors_std_figure, 
                 bar_chart_permission_errors_std_figure, 
                 scenery_name_children,
                 load_essay_dropdown_value
                 ):

    rli2 = RLI2()
    
    essay_gauge_value = essay_episodes_slider * essay_spe_slider

    finished_file = os.path.join("results","finished.txt")
    attack_button_disabled = True
    
    scenery_dropdown_value = "NASIM:" + scenery_name_children

    if load_essay_dropdown_value != "Empty":

        scenery_dropdown_value = load_essay_dropdown_value[0:load_essay_dropdown_value.rfind('-')]
        scenery_dropdown_value = scenery_dropdown_value[0:scenery_dropdown_value.rfind('-')]
        scenery_dropdown_value = scenery_dropdown_value[0:scenery_dropdown_value.rfind('-')]
        scenery_name_children = scenery_dropdown_value.replace("NASIM:results-","")
        scenery_dropdown_value = scenery_dropdown_value.replace("results-","")

        results = os.path.join("results","results.json")
        fasta = os.path.join("results","results.fasta")
        finished = os.path.join("results","finished.txt")
        if os.path.exists(results):
            os.remove(results)
        if os.path.exists(fasta):
            os.remove(fasta)
        if os.path.exists(finished):
            os.remove(finished)

        results_path = os.path.join("results",load_essay_dropdown_value.replace("NASIM:",""))
        shutil.copyfile(results_path+".json", results)            
        shutil.copyfile(results_path+".fasta", fasta)            
        
        with open(finished_file, 'w') as fp:
            pass

    if os.path.exists(finished_file):
    
        if load_essay_dropdown_value == "Empty":
    
            interval_component_disabled  = True
            attack_button_disabled = False
            attack_button_children = "Attack!"

        load_essay_dropdown_value = "Empty"

        os.remove(finished_file)

        results_file = os.path.join("results","results.json")
        
        df = pd.read_json(results_file)

        
        df['steps_MA10'] = df.groupby('serie')['last_step'].transform(lambda x: x.rolling(10, 1).mean())
        df['rewards_MA10'] = df.groupby('serie')['reward'].transform(lambda x: x.rolling(10, 1).mean())

        df['successes_rate'] = df['successes'] / df['last_step']
        df['connection_errors_rate'] = df['connection_errors'] / df['last_step']
        df['permission_errors_rate'] = df['permission_errors'] / df['last_step']

        df['successes_rate_MA10'] = df.groupby('serie')['successes_rate'].transform(lambda x: x.rolling(10, 1).mean())
        df['connection_errors_rate_MA10'] = df.groupby('serie')['connection_errors_rate'].transform(lambda x: x.rolling(10, 1).mean())
        df['permission_errors_rate_MA10'] = df.groupby('serie')['permission_errors_rate'].transform(lambda x: x.rolling(10, 1).mean())

        df_successes_rate_std = pd.DataFrame()
        df_connection_errors_rate_std = pd.DataFrame()
        df_permission_errors_rate_std = pd.DataFrame()
        '''
        df_successes_rate_std["std"] = df[df['episode'] <= 20].groupby(['serie']).successes_rate.std()
        df_connection_errors_rate_std["std"] = df[df['episode'] <= 20].groupby(['serie']).connection_errors_rate.std()
        df_permission_errors_rate_std["std"] = df[df['episode'] <= 20].groupby(['serie']).permission_errors_rate.std()
        '''
        df_successes_rate_std["std"] = df.groupby(['serie']).successes_rate.std()
        df_connection_errors_rate_std["std"] = df.groupby(['serie']).connection_errors_rate.std()
        df_permission_errors_rate_std["std"] = df.groupby(['serie']).permission_errors_rate.std()
        
        df_successes_rate_std['serie'] = df_successes_rate_std.index.values
        df_connection_errors_rate_std['serie'] = df_connection_errors_rate_std.index
        df_permission_errors_rate_std['serie'] = df_permission_errors_rate_std.index
        

        scatter_chart_successes_figure = px.scatter(
                df[df['episode'] == 1],
                x="last_step",
                y="successes_rate",
                color="serie",
                title=f'First episode success rates in the {scenery_name_children}',
                # text="serie",
        )

        scatter_chart_connection_errors_figure = px.scatter(
                df[df['episode'] == 1],
                x="last_step",
                y="connection_errors_rate",
                color="serie",
                title=f'First episode connection errors in the {scenery_name_children}',
                # text="serie",
        )

        scatter_chart_permission_errors_figure = px.scatter(
                df[df['episode'] == 1],
                x="last_step",
                y="permission_errors_rate",
                color="serie",
                title=f'First episode permission errors in the {scenery_name_children}',
                # text="serie",
        )

        line_chart_steps_figure = px.line(
                df,
                x="episode",
                y="last_step",
                color="serie",
                line_group="serie",
                title=f'Steps by episodes in the {scenery_name_children}',

        )

        line_chart_score_figure = px.line(
                df,
                x="episode",
                y="reward",
                color="serie",
                line_group="serie",
                title=f'Rewards by episodes in the {scenery_name_children}',
        )

        line_chart_stepsmm_figure = px.line(
                df,
                x="episode",
                y="steps_MA10",
                color="serie",
                line_group="serie",
                title=f'Moving average (10) steps by episodes in the {scenery_name_children}',

        )

        line_chart_scoremm_figure = px.line(
                df,
                x="episode",
                y="rewards_MA10",
                color="serie",
                line_group="serie",
                title=f'Moving average (10) rewards by episodes in the {scenery_name_children}',
        )

        line_chart_successes_figure = px.line(
                df,
                x="episode",
                y="successes_rate",
                color="serie",
                line_group="serie",
                title=f'Success rates by episodes in the {scenery_name_children}',

        )

        line_chart_connection_errors_figure = px.line(
                df,
                x="episode",
                y="connection_errors_rate",
                color="serie",
                line_group="serie",
                title=f'Connection errors rates by episodes in the {scenery_name_children}',
        )

        line_chart_permission_errors_figure = px.line(
                df,
                x="episode",
                y="permission_errors_rate",
                color="serie",
                line_group="serie",
                title=f'Permission errors rates by episodes in the {scenery_name_children}',
        )


        line_chart_successes_ma_figure = px.line(
                df,
                x="episode",
                y="successes_rate_MA10",
                color="serie",
                line_group="serie",
                title=f'Moving average (10) success rates by episodes in the {scenery_name_children}',
        )

        line_chart_connection_errors_ma_figure = px.line(
                df,
                x="episode",
                y="connection_errors_rate_MA10",
                color="serie",
                line_group="serie",
                title=f'Moving average (10) connectoin errors rates by episodes in the {scenery_name_children}',
        )

        line_chart_permission_errors_ma_figure = px.line(
                df,
                x="episode",
                y="permission_errors_rate_MA10",
                color="serie",
                line_group="serie",
                title=f'Moving average (10) permission errors rates by episodes in the {scenery_name_children}',
        )
        bar_chart_successes_ma_figure = px.histogram(
                df,
                x="serie",
                y="successes_rate",
                color="serie",
                barmode="group",
                histfunc='avg',
                title=f'Average from success rates of episodes in the {scenery_name_children}'
        )
        bar_chart_connection_errors_ma_figure = px.histogram(
                df,
                x="serie",
                y="connection_errors_rate",
                color="serie",
                barmode="group",
                histfunc='avg',
                title=f'Average from connection errors rates of episodes in the {scenery_name_children}'
        )
        bar_chart_permission_errors_ma_figure = px.histogram(
                df,
                x="serie",
                y="permission_errors_rate",
                color="serie",
                barmode="group",
                histfunc='avg',
                title=f'Average from permission errors rates of episodes in the {scenery_name_children}'
        )
        bar_chart_successes_std_figure = px.histogram(
                df_successes_rate_std,
                x="serie",
                y="std",
                color="serie",
                barmode="group",
                title=f'Standard deviation from success rates of episodes in the {scenery_name_children}'
        )
        bar_chart_connection_errors_std_figure = px.histogram(
                df_connection_errors_rate_std,
                x="serie",
                y="std",
                color="serie",
                barmode="group",
                title=f'Standard deviation from connection errors rates of episodes in the {scenery_name_children}'
        )
        bar_chart_permission_errors_std_figure = px.histogram(
                df_permission_errors_rate_std,
                x="serie",
                y="std",
                color="serie",
                barmode="group",
                title=f'Standard deviation from permission errors rates of episodes in the {scenery_name_children}'
        )

        results_table_data = df.to_dict('records')

    elif series_checklist_value == []:
        attack_button_disabled = True
        attack_button_n_clicks = None

    elif attack_button_children == "Attack!":
        attack_button_disabled = False

        if attack_button_n_clicks is not None: 
            attack_button_disabled = True
            attack_button_n_clicks = None
            interval_component_disabled  = False
            attack_button_children = [dbc.Spinner(size="sm"), " Attacking..."]
            attack_process = multiprocessing.Process(target=rli2.attack, args=(scenery_name_children, series_checklist_value, essay_gauge_value, essay_spe_slider))
            attack_process.start()


    return essay_gauge_value, \
           attack_button_disabled, \
           attack_button_n_clicks, \
           attack_button_children, \
           results_table_data, \
           scatter_chart_successes_figure, \
           scatter_chart_connection_errors_figure, \
           scatter_chart_permission_errors_figure, \
           line_chart_steps_figure, \
           line_chart_score_figure, \
           line_chart_stepsmm_figure, \
           line_chart_scoremm_figure, \
           line_chart_successes_figure, \
           line_chart_connection_errors_figure, \
           line_chart_permission_errors_figure, \
           line_chart_successes_ma_figure, \
           line_chart_connection_errors_ma_figure, \
           line_chart_permission_errors_ma_figure, \
           bar_chart_successes_ma_figure, \
           bar_chart_connection_errors_ma_figure, \
           bar_chart_permission_errors_ma_figure , \
           bar_chart_successes_std_figure, \
           bar_chart_connection_errors_std_figure, \
           bar_chart_permission_errors_std_figure, \
           interval_component_disabled, \
           load_essay_dropdown_value, \
    

#################################
# RLI2LAB - Web server
##################################
if __name__ == "__main__":
    app.run_server()